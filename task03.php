<?php
class Calculator {
    private $value1, $value2;


    public function __construct( $value1, $value2 ) {
        $this->value1 = $value1;
        $this->value2 = $value2;
    }

    public function add() {
        return $this->value1 + $this->value2;
    }


    public function subtract() {
        return $this->value1 - $this->value2;
    }

    public function multiply() {
        return $this->value1 * $this->value2;
    }

    public function divide() {
        return $this->value1 / $this->value2;
    }
}
$mycalc = new Calculator(20, 4);
echo "Add: ";
echo $mycalc-> add().'<br>';
echo "Multi: ";
echo $mycalc-> multiply().'<br>';
echo "Sub: ";
echo $mycalc-> subtract().'<br>';
echo "Div: ";
echo $mycalc-> divide();
?>
