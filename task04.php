<?php

class factorial_of_a_number
{
    protected $_number;

    public function __construct($number)
    {
        if (!is_int($number)) {
            throw new InvalidArgumentException('Not a number or missing argument');
        }
        $this->_number = $number;
    }
    public function result()
    {
        $number=$this->_number;
        function factorial($number) {

            if ($number < 2) {
                return 1;
            } else {
                return ($number * factorial($number-1));
            }
        }
        return factorial($number);
    }
}

$newfactorial = New factorial_of_a_number(5);
echo $newfactorial->result();
?>